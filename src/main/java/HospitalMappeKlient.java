import HospitalAndDepartments.Department;
import HospitalAndDepartments.Hospital;
import HospitalAndDepartments.RemoveException;
import Persons.Employee;
import Persons.Patient;
import Persons.Person;

public class HospitalMappeKlient {
    public static void main(String[] args) {

        Hospital haukeland = new Hospital("Haukeland");

        HospitalTestData.fillRegisterWithTestData(haukeland);

        Employee testEmployee = new Employee("Odd Even", "Primtallet", "1");

        /**
         * Dette er måten jeg bruker remove metoden som er i Employee klassen. Jeg må først sjekke at
         * det er rett department, før jeg da prøver å bruke remove metoden. Om den klarer det skriver den
         * at det var vellykket. Hvis ikke "catcher" den RemoveException, og skriver ut det som blir
         * catchet.
         */
        try {
            for(Department dep : haukeland.getDepartments()) {
                if(dep.getDepartmentName().equals("Akutten")) {
                    dep.remove(testEmployee);
                    System.out.println("Arbeider er vellykket fjernet \n");
                }
            }
        } catch (RemoveException e) {
            System.out.println(e);
        }


        /**
         * Dette er skrevet på samme måte som kodelinjen over. Forskjellen er at jeg her sjekker
         * at metoden også fungerer når inputet er feil (pasienten ikke finnes i listen til department"
         */
        Patient wrongPatient = new Patient("Wrong", "Wrong2", "123");

        try {
            for(Department dep : haukeland.getDepartments()) {
                if(dep.getDepartmentName().equals("Akutten")) {
                    dep.remove(wrongPatient);
                    System.out.println("Pasient er vellykket fjernet.");
                }
            }
        } catch (RemoveException e) {
            System.out.println(e);
        }
    }
}
