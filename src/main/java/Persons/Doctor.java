package Persons;

/**
 * Dette er Doctor klassen. Dette er en abstrakt klasse som extender Employee klassen.
 * Dette er for å sortere at "Doctor" er employees. Vi ser dette da vi bruker
 * "super" i konstruktøren, som er for å kalle på superklassen, altså Employee.
 */

public abstract class Doctor extends Employee {
    public Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
