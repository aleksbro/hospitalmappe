package Persons;

/**
 * Surgeon er en sub-klasse av doctor.
 */
public class Surgeon extends Doctor{

    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     *
     * @param patient
     * @param diagnosis
     *
     * Surgeon har som vi ser i denne metoden, muligheten til å sette diagnose på pasienter.
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
