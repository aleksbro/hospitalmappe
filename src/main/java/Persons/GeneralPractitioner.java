package Persons;

/**
 * Dette er GeneralPractitioner klassen, som extender Doctor klassen.
 * Dette er for å igjen kunne sortere Doctor på en god måte.
 */
public class GeneralPractitioner extends Doctor{

    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
    super(firstName, lastName, socialSecurityNumber);
    }

    /**
     *
     * @param patient
     * @param diagnosis
     * Metode som setter diagnosen til Patient.
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
