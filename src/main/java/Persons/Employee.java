package Persons;

/**
 * Dette er Employee klassen. Denne extender Person klassen. Dette er fordi alle som arbeider
 * på sykehuset må bli laget som personer. Man ser at man kaller på super klassen med "super".
 */
public class Employee extends Person {

    public Employee(String firstName, String lastName, String socialSecurityNumber) {
    super(firstName, lastName, socialSecurityNumber);
    }

    /**
     *
     * @return String
     * toString metode som bestemmer hvordan Employees skal bli skrevet ut.
     */
    @Override
    public String toString() {
        return "Name: " + this.getFullName() + "\n" +
                "Position: employee" + "\n" +
                "Social security number: " + this.getSocialSecurityNumber() + "\n\n";
    }
}
