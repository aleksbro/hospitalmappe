package Persons;

import java.util.Objects;


/**
 * Klassen Person er en abstract klasse. Denne skal bli subklasset. Vi lager dette som en
 * abstrakt klasse for å kunne implementere de egenskapene som er felles for alle personer.
 * Sub-klasser blir deretter laget for å legge til de funksjonene som er spesielle for sub-klassene.
 */
public abstract class Person {

    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     *
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     *
     * Dette er konstruktøren. Denne passer på at man ikke skriver noen blanke felter.
     * Om programmet skulle vært realisert ville jeg nok lagt inn en sjekk på at personnummeret er så langt som det skal være.
     * Dette valgte jeg å ikke gjøre, da jeg så at eksemplene vi har fått tildelt ikke ville møte dette kravet.
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        if(firstName == null || firstName.isBlank()) {
            throw new IllegalArgumentException("First name can't be blank.");
        }
            this.firstName = firstName;

        if(lastName == null || lastName.isBlank()) {
            throw new IllegalArgumentException("Last name can't be blank");
        }
        this.lastName = lastName;

        if(socialSecurityNumber == null || socialSecurityNumber.isBlank()) {
            throw new IllegalArgumentException("Security number can't be blank");
        }
        this.socialSecurityNumber = socialSecurityNumber;
    }


    /**
     *
     * @param o
     * @return boolean
     *
     * Denne equals metoden definerer hvordan vi ønsker å sammenligne objektene som er Person.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return getFirstName().equals(person.getFirstName()) && getLastName().equals(person.getLastName()) && getSocialSecurityNumber().equals(person.getSocialSecurityNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getSocialSecurityNumber());
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     *
     * @return String
     *
     * Metoden setter sammen fornavn og etternavn slik at man ikke trenger å kalle
     * på begge deler for å hente hele navnet.
     */
    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    /**
     *
     * @return String
     *
     * toString metoden definerer hvordan Person blir skrevet ut. Har her en hjelpemetode
     * som heter "getFullName()". Denne bruker jeg også i toString metoden min.
     */
    public String toString() {
        return "Name: " + this.getFullName() + "\n" +
                "Social security number: " + this.socialSecurityNumber + "\n\n";
    }





}
