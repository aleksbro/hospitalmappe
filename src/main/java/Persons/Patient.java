package Persons;

/**
 * Patient er pasientene som er i de forskjellige departmentsene.
 * Patient er en person, og der dermed også en sub-klasse av Person.
 * Vi ser at Patient har en spesiell egenskap som er å ha en diagnose.
 * Da har vi implementert Diagnosable for å kunne sette en diagnose på Patient.
 */
public class Patient extends Person implements Diagnosable{
    private String diagnosis = "";

    public Patient(String firsName, String lastName, String socialSecurityNumber) {
        super(firsName, lastName, socialSecurityNumber);
    }

    public String getDiagnosis() {
        return this.diagnosis;
    }

    /**
     *
     * @param diagnosestilling på jobb
     *
     * Void metode for å kunne sette en diagnose på Patient.
     */
    public void setDiagnosis(String diagnose) {
        this.diagnosis = diagnose;
    }

    @Override
    public String toString() {
        return "Name: " + this.getFullName() + "\n" +
                "Position: patient" + "\n" +
                "Social security number: " + this.getSocialSecurityNumber() + "\n" +
                "Diagnosis: " + this.diagnosis + "\n\n";
    }
}