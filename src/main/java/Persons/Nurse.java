package Persons;

/**
 * For å være en Nurse, må man også være en employee. Nurse er derfor
 * extended fra Employee klassen. Man ser at Nurse er en sub-klasse.
 */
public class Nurse extends Employee {

    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     *
     * @return String
     * toString metode som bestemmer hvordan Persons skal bli skrevet ut
     */
    @Override
    public String toString()  {
        return "Name: " + this.getFullName() + "\n" +
                "Position: nurse " + "\n" +
                "Social security number: " + this.getSocialSecurityNumber() + "\n\n";
    }
}
