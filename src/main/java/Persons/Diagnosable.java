package Persons;

/**
 * Dette er Diagnosible klassen og er en interface klasse. Dette betyr at denne klassen er
 * helt abstrakt. Denne klassen er mest for å sortere programmet på en god måte.
 * Klassen er implimentert i Patient klassen
 */

interface Diagnosable {

    public void setDiagnosis(String diagnosis);
}
