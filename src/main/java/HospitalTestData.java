
        import HospitalAndDepartments.Department;
        import HospitalAndDepartments.*;
        import Persons.*;

public final class HospitalTestData {
    private HospitalTestData() {
        // not called
    }
    /**
     * @param hospital
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        Department department1 = new Department("Department1");
        Department emergency = new Department("Akutten");

        emergency.getEmployees().add(new Employee("Odd Even", "Primtallet", "1"));
        emergency.getEmployees().add(new Employee("Huppasahn", "DelFinito", "2"));
        emergency.getEmployees().add(new Employee("Rigmor", "Mortis", "3"));
        emergency.getEmployees().add(new GeneralPractitioner("Inco", "Gnito", "4"));
        emergency.getEmployees().add(new Surgeon("Inco", "Gnito", "5"));
        emergency.getEmployees().add(new Nurse("Nina", "Teknologi", "6"));
        emergency.getEmployees().add(new Nurse("Ove", "Ralt", "7"));
        emergency.getPatients().add(new Patient("Inga", "Lykke", "8"));
        emergency.getPatients().add(new Patient("Ulrik", "Smål", "9"));
        hospital.getDepartments().add(emergency);

        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.getEmployees().add(new Employee("Salti", "Kaffen", "12"));
        childrenPolyclinic.getEmployees().add(new Employee("Nidel V.", "Elvefølger", "13"));
        childrenPolyclinic.getEmployees().add(new Employee("Anton", "Nym", "14"));
        childrenPolyclinic.getEmployees().add(new GeneralPractitioner("Gene", "Sis", "15"));
        childrenPolyclinic.getEmployees().add(new Surgeon("Nanna", "Na", "16"));
        childrenPolyclinic.getEmployees().add(new Nurse("Nora", "Toriet", "17"));
        childrenPolyclinic.getPatients().add(new Patient("Hans", "Omvar", "18"));
        childrenPolyclinic.getPatients().add(new Patient("Laila", "La", "19"));
        childrenPolyclinic.getPatients().add(new Patient("Jøran", "Drebli", "20"));
        hospital.getDepartments().add(childrenPolyclinic);
    }
}
