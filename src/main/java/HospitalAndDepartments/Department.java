package HospitalAndDepartments;

import Persons.Employee;
import Persons.Patient;
import Persons.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * Dette er department klassen. den lager et department og lagrer de som jobber der og
 * de som er pasienter der.
 */
public class Department {
    private String departmentName;
    private final ArrayList<Employee> employees;
    private final ArrayList<Patient> patients;

    /**
     *
     * @param departmentName
     * Konstruktør som gjør at det eneste du trenger for å lage et nytt "Department" er et navn.
     * Sjekker her at navnet ikke er tomt.
     */
    public Department(String departmentName) {
        if(departmentName == null || departmentName.isBlank()) {
            throw new IllegalArgumentException("Department name can't be blank");
        }
        this.departmentName = departmentName;
        this.employees = new ArrayList<Employee>();
        this.patients = new ArrayList<Patient>();
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentName() {
        return departmentName;
    }


    public List<Employee> getEmployees() {
        return employees;
    }

    /**
     *
     * @param employee
     *
     * Metode for å legge til ny employee. Sjekker først om employee finnes fra før.
     */
    public void addEmployee(Employee employee) {
        if(employees.contains(employee)) {
            throw new IllegalArgumentException("This employee already exists");
        }
        employees.add(employee);
    }

    public List<Patient> getPatients() {
        return patients;
    }

    /**
     *
     * @param patient
     * Metoden legger til ny Patient. Sjekker først at denne Patient ikke finnes fra før.
     */
    public void addPatient(Patient patient) {
        if(patients.contains(patient)) {
            throw new IllegalArgumentException("This patient already exists");
        }
        patients.add(patient);
    }

    /**
     * @return boolean
     * @param o
     * En equals metode som gjør at man kan sammenligne innholdet i department med hverandre.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Department)) return false;
        Department that = (Department) o;
        return Objects.equals(getDepartmentName(), that.getDepartmentName()) && Objects.equals(getEmployees(), that.getEmployees()) && Objects.equals(getPatients(), that.getPatients());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDepartmentName(), getEmployees(), getPatients());
    }

    /**
     *
     * @param person
     * @return boolean
     * @throws RemoveException
     *
     * Dette er en metode som gjør at man kan fjerne objekt av typen Patient eller Employee
     * fra deres lister. Jeg har valgt å gjøre dette til en boolean, da det er veldig greit
     * å kunne få en tilbakemelding på om metoden har gjennomført det den skulle.
     * I tillegg har jeg laget en egen exception klasse som har som ansvar å håndtere
     * den exception som blir kastet om metoden ikke finner objektet i noen av listene.
     */
    public boolean remove(Person person) throws RemoveException {
        boolean removed = false;

        if(person instanceof Patient) {
            if (!(patients.contains(person))) {
                throw new RemoveException("Denne pasienten finnes ikke");
            } else {
                patients.remove(person);
                removed = true;
            }

        } else if(person instanceof  Employee) {
            if(!(employees.contains(person))) {
                throw new RemoveException("Denne ansatte finnes ikke");
            } else {
                employees.remove(person);
                removed = true;
            }
        }

        return removed;
    }

    /**
     *
     * @return String
     *
     * Dette er en enkel toString metode som bestemmer hvordan innholdet i et department
     * skal bli skrevet ut.
     */
    public String toString() {
        return "Department name: " + departmentName + "\n" + "\n"+
                "Employees: " + employees + "\n" + "\n" +
                "Patients: " + patients + "\n" + "\n";
    }
}
