package HospitalAndDepartments;

/**
 * Dette er RemoveException klassen. Denne er laget for å håndtere exceptionsene som blir
 * sendt fra remove metoden i Department.
 */
public class RemoveException extends Exception{

    static final long serialVersionUID = 1L;

    public RemoveException(String message) {
        super(message);
    }


}
