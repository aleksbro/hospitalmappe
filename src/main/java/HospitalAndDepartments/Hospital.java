package HospitalAndDepartments;

import java.util.ArrayList;
import java.util.List;

/**
 * Dette er Hospital klassen. Denne klassen oppretter et Hospital og lagrer
 * de departmentsene som skal ligge her. Disse blir lagret i en arrayList.
 *
 */

public class Hospital {

    private final String hospitalName;
    private final ArrayList<Department> departments;


    /**
     *
     * @param hospitalName
     *
     * Dette er konstruktøren, man trenger bare et navn for å lage et hospital.
     */
    public Hospital(String hospitalName) {
        if(hospitalName == null || hospitalName.isBlank()) {
            throw new IllegalArgumentException("Hospital name can't be blank");
        }
        this.hospitalName = hospitalName;
        this.departments = new ArrayList<Department>();
    }

    public String getHospitalName() {
        return this.hospitalName;
    }

    public List<Department> getDepartments() {
        return departments;
    }
}
