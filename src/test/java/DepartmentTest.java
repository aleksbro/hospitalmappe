import Persons.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import HospitalAndDepartments.*;
import Persons.*;

import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {

    Department department1;
    Employee employee1;
    Patient patient1;



    @BeforeEach
    public void setUp() {
        department1 = new Department("Department1");

        employee1 = new Employee("Aleksander", "Røed", "123");
        patient1 = new Patient("Nicolai", "Sivesind", "12345");

        department1.addEmployee(employee1);
        department1.addPatient(patient1);
    }

    /**
     *
     * @throws RemoveException
     * Tester om det å remove employee fungerer som det skal. Assert true skal bli godkjent
     * om remove klassen fungerer som den skal.
     */
    @Test
    public void testRemoveEmployee() throws RemoveException {
        assertTrue(department1.remove(employee1));
    }

    /**
     *
     * @throws RemoveException
     * Prøver å remove Patient. Skal bli godkjent om remove metoden fungerer som den skal.
     */
    @Test
        public void testRemovePatient() throws RemoveException {
            assertTrue(department1.remove(patient1));
        }


    /**
     * Tester å fjerne en employye som ikke finnes i employee listen. Velger her å deklarere Employee
     * lokalt i metoden for å gi en ryddighet, da denne bare blir brukt en gang. Metoden catcher
     * exception som blir sendt og skriver denne ut.
     */
    @Test
    public void testRemoveFail() {
            Employee wrong = new Employee("a", "b", "123");

            try {
                assertFalse(department1.remove(wrong));
            } catch (RemoveException e) {
                System.out.println(e);
            }
        }

}